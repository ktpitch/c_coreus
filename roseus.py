####################################################
# This module is a collection of functions used to #
# analysize and to synthesize Khlui-Phiang-Aw (KPN)#
# sound.                                           #
####################################################
# Kittipitch Meesawat                              #
# mkittiphong@kku.ac.th                            #
# 18 Feb 2019                                      #
####################################################

# Import section
import numpy as np
import scipy.io.wavfile
from scipy import signal
# Import for debuging purpose
#from matplotlib import pyplot as plt
# Constants

# Classes definition
class Note:
    fL = 200 # Bandpass filter specification for analysis only
    fH = 2000 # Bandpass filter specification for analysis only
    bpass_order = 4
    N = 4096 # N-points for psd routine
    Prominence = 25

    # Add target fs to accomodate various sampling frequency
    def __init__(self,infile,fs):
        # Initialize the Note with a filename
        # First and foremost attribites of the Note are fs and data
        fs0, data = scipy.io.wavfile.read(infile)
        if (fs0!=fs):
            gcd = np.gcd(fs0,fs)
            if fs0 > fs:
                up = fs/gcd
                down = fs0/gcd
            else:
                up = fs0/gcd
                down = fs/gcd

            data = signal.resample_poly(data,up,down)

        #print("up = ",up," \t down = ",down)
        # Lowpass filter to remove low frequency noise is inserted
        self.fs = fs
        fL = 100/(self.fs/2)
        B, A = signal.butter(4,fL,'highpass',analog=False,output='ba')
        data = signal.lfilter(B, A, data)

        self.data = data

        # The normalization is questionable. It might make noise floor uneven.
        # self.data = data/np.max(np.abs(data)) # This normalize is questioned!

        # Bandpass the data
        # This is for psd analysis only
        # so that the peak finder does not have to work too hard on HF peaks.
        locut = self.fL/(self.fs/2)
        hicut = self.fH/(self.fs/2)
        B, A = signal.butter(self.bpass_order,[locut,hicut],'bandpass',analog=False,output='ba')
        filsig = signal.lfilter(B,A,self.data)

        # Find peaks from the signal's psd, the first one is the pitch
        # The peaks are converted to fourier coefs.
        f, psd = signal.welch(filsig,fs=self.fs,window='blackmanharris',nperseg=self.N,detrend='linear',scaling='spectrum')
        PSD = 10*np.log10(np.abs(psd))
        # Ploting for debuging purpose
        #plt.semilogx(f,PSD)
        #plt.xlim((100,1000))
        #plt.grid(which='both')
        peakidx = signal.find_peaks(PSD,prominence=self.Prominence)
        peakidx = peakidx[0][:] # Convert from tuple to array
        pitch = f[peakidx][0]
        self.c_kpn = np.sqrt(2)*np.sqrt(psd[peakidx]) # Fourier coefficients from psd
        self.pitch = pitch

        # Use psd to determine an appropriate gain for noise filter
        # Take only psd at frequency lower than the pitch
        # Assume one octave bandwidth.
        # Another psd for raw signal, not lowpassed.
        f, psdraw = signal.welch(data,fs=self.fs,window='blackmanharris',nperseg=self.N,detrend='linear',scaling='spectrum')
        flast = np.argwhere(f<(pitch/np.sqrt(2)))[-1][0] # NP indexing is headache!
        pmax = np.max(psdraw[0:flast])
        #vmax = np.sqrt(pmax)*np.sqrt(2)*(fs/self.N)
        vmax = np.sqrt(pmax)*2*np.sqrt(2)*(self.fs/self.N)
        self.noise_gain = vmax

        # List of harmonic components
        harmcomp = np.round(f[peakidx]/self.pitch)
        harmcomp =harmcomp.astype(int) # Convert to integer
        self.harmcomp = harmcomp

        # Locate the fisrt missing harminic
        # The following fragment return WRONG alpha for pitch no.10 and 11
        # diffharm = np.diff(harmcomp) # Step to the next harmonic
        # skip = np.argmax(diffharm) # Step that greater than "1"
        # if skip == 0:
        #     missharm = len(harmcomp) + 1
        # else:
        #     missharm = skip + 2 # Convert to the missing harmonic number

        # The following is the fixed (trial) version of the first
        # missing harmonic locator
        # Create integer list
        harmcheck = np.arange(len(harmcomp)) + 1
        check = np.nonzero(harmcomp - harmcheck)
        # Extract the first nonzero element from the array
        if check[0].size == 0:
            missharm = len(harmcomp) + 1
        else:
            missharm = check[0][0]+1

        # Compute the alpha, the asymmetry factor
        self.alpha = 1/missharm

class Resonator:
    MIDHARM = 3 # Middle harmonic, choose the third harmonic in this project
    KPNORDER = 13 # This is number of taps not order
    GPASS = 3 # Passband gain of the main lowpass filter
    GSTOP = 20 # Stopband gain of the main lowpass filter

    def __init__(self,note):
        # Initialize the resonator with the prototype note
        self.fs = note.fs
        c_kpn = note.c_kpn

        # Gain at particular pre-warped frequencies. Refer to page 32 of the report
        harmcomp = note.harmcomp
        alpha = note.alpha
        c_tri = (2*np.abs(np.sin(harmcomp*np.pi*alpha))) / (((np.pi*harmcomp)**2)*alpha*np.abs((alpha-1)))
        gain = c_kpn/c_tri

        # These two may not needed to be public
        self.c_tri = c_tri
        self.gain = gain

        # Frequency specification for warped filter
        w = ((note.harmcomp * note.pitch)/note.fs)*2*np.pi
        wp, gain, gamma = self.__firspec(w,gain)
        weight = np.ones(int(len(wp)/2)) # Weighting factor (half size of BW)
        weight[1] = 2 # Higher weighting factor at the fundamental frequency

        # Filter design
        fp = (wp/(2*np.pi))*self.fs
        coef = signal.firls(self.KPNORDER,fp,gain,weight,fs=self.fs)

        # This may not needed to be public.
        self.coef = coef

        # Dewarping and save num and den for the most important object attributes, num and den for the IIR resonator
        num, den = self.__dewarp(coef,gamma)
        self.num = num
        self.den = den

        # Design a lowpass with wp = the last harmonic and ws is the next harmonic to the last one. They are not merge (i.e. convolve) with the resonator filter for the sake of modular design.
        blp1, alp1 = self.__main_lowpass(note)
        self.blp1 = blp1
        self.alp1 = alp1

    def __ftonorm(self,freq):
        # Converting to normalized frequency (fs/2 is 1)
        normfreq = freq/(self.fs/2)
        return normfreq

    def __warpfactor(self,w,wp=np.pi*0.5):
        # Calculate the warping factor so that the third harmonic frequency is mapped to the (fs/2)/2
        # Refer to page 41 in the report
        arg = (w-wp)/2
        t = np.tan(arg)
        c = np.cos(w)
        s = np.sin(w)
        gamma = t/(-s+(c*t))
        return gamma

    def __warping(self,w,gamma):
        # Warp the target frequencies to the warped domain using gamma
        # Refer to page 39 in the report
        s = np.sin(w)
        c = np.cos(w)
        t = 2*np.arctan(-gamma*s/(1+(-gamma*c)))
        wp = w - t
        return wp

    def __firspec(self,w,gain):
        gamma = self.__warpfactor(w[0]*self.MIDHARM)
        wp = self.__warping(w,gamma)

        # Insert DC gain at the beginning of the target
        wp = np.insert(wp,0,0)
        gain = np.insert(gain,0,gain[0])

        # Insert 0 at the the end of the target frequency axis
        wp = np.append(wp,np.pi)
        gain = np.append(gain,0)

        # Packing and arrange the data for firls by interleaving data
        # Ex. w_origin = [w0 w1 w2] the result should be
        # w_result = [w0 w1 w1 w2] to reflect the bands w0-w1 and w1-w2 clearly.
        wq = np.roll(wp,-1)
        wtemp = [[p,q] for p,q,in zip(wp,wq)]
        wp = np.asarray(wtemp[:-1]).flatten()

        # Same as above
        gainq = np.roll(gain,-1)
        gaintemp = [[p,q] for p,q in zip(gain,gainq)]
        gain = np.asarray(gaintemp[:-1]).flatten()

        return wp, gain, gamma

    def __dewarp(self,coef,gamma):
        # Dewarping the FIR filter back to its pre-warped domain
        # Which is simply done by substituting the z^{-n} by the allpass filter
        # defined on page 42 in the report
        N = len(coef)
        num = np.array([-gamma,1])
        den = np.array([1,-gamma])

        overall = np.zeros(len(coef))
        for i in range(N):
            cumnum = np.array([1])
            cumden = np.array([1])

            for j in range(i):
                cumnum = np.convolve(cumnum,num)
            for k in range(N-i-1):
                cumden = np.convolve(cumden,den)

            cumnum = np.convolve(cumnum,cumden)
            cumnum = cumnum * coef[i]
            overall = overall + cumnum

        cumden = np.array([1])
        for i in range(N-1):
            cumden = np.convolve(cumden,den)

        return overall, cumden

    def __main_lowpass(self,note):
        # wp and ws were normalized to pi,
        # but from the manual they should be normalized to 1 instead.
        wp = self.__ftonorm(note.harmcomp[-1]*note.pitch) # The last harmonic
        ws = self.__ftonorm((note.harmcomp[-1]+1)*note.pitch) # The next to the last
        # Debug
        #print("wp = ",wp,"\t ws = ",ws)
        order, wn = signal.cheb2ord(wp,ws,self.GPASS,self.GSTOP)
        blp1,alp1 = signal.cheby2(order,self.GSTOP,wn)
        return blp1, alp1

class WindResonator:
    FPASS = 5000
    FSTOP = 7000
    GPASS = 3
    GSTOP = 40
    def __init__(self,note,Q=4.318):
        # Initialize the object with note. Q and gain become the object parameters because it is not final what Q and gain should be.
        wlist = (note.harmcomp*note.pitch)/(note.fs/2)
        # There are n resonators where n is the number of significant harmonics
        for i, wc in enumerate(wlist):
            b, a = signal.iirpeak(wc,Q)
            # All wind resonators are in parallel
            if i==0:
                bcum = b
                acum = a
            else:
                bcum1 = np.convolve(bcum,a)
                bcum2 = np.convolve(acum,b)
                bcum = bcum1 + bcum2
                acum = np.convolve(acum,a)

        gain = note.noise_gain
        self.num = bcum*gain
        self.den = acum

        # Let this class take care of the final lowpass filter
        wp = self.FPASS/(note.fs/2)
        ws = self.FSTOP/(note.fs/2)
        order,wn = signal.cheb2ord(wp,ws,self.GPASS,self.GSTOP)

        blp2,alp2 = signal.cheby2(order,self.GSTOP,wn)
        self.blp2 = blp2
        self.alp2 = alp2
